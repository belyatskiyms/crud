import React from 'react';
import Rows from './Rows';
class Table extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            information : [],
        }
        this.handlerAdd = this.handlerAdd.bind(this);
        this.handlerRemove = this.handlerRemove.bind(this);
        this.handlerChange = this.handlerChange.bind(this);
        this.handlerSave = this.handlerSave.bind(this);
    }
    handlerChange(value, index, key) {
        let newInformation = this.state.information.slice()
        newInformation[index][`data`][`${key}`] = value
        this.setState({information : newInformation})
    }
    handlerSave(id, index){
        let newElement = {
            data: this.state.information[index]['data']
        }
        fetch(`http://178.128.196.163:3000/api/records/${id}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
        body: JSON.stringify(newElement)
        })
    }
    handlerRemove(id){
        fetch(`http://178.128.196.163:3000/api/records/${id}`, {
        method: 'DELETE',
        }).then(()=> this.addDataFromServer())
    }
    handlerAdd(){
        let newElement={
            data: {}
        }
        Object.keys(this.state.information[0].data).forEach((key)=>{
            newElement.data[key] = ''
        })
        fetch('http://178.128.196.163:3000/api/records/', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
        body: JSON.stringify(newElement)
        })
        .then(() => this.addDataFromServer())
    }
    addDataFromServer(){
        fetch('http://178.128.196.163:3000/api/records/')
        .then(response => response.json())
        .then(date => this.setState({information : date}));
    }
    componentDidMount(){
        this.addDataFromServer()
    }
    render() {
        let rows = this.state.information.map((item, index)=>{
            return (<Rows 
            rowsDate = {item}
            rowsIndex = {index} 
            handlerChange = {this.handlerChange}
            handlerRemove={this.handlerRemove}
            handlerSave = {this.handlerSave} 
            key = {index}
            />)
        })
        
        return (
        <div>
            <table className="table">
                <tbody>
                    {rows}
                </tbody>
            </table>
            <button onClick={this.handlerAdd}>Добавить</button>
        </div>
    )}
  }
  export default Table;
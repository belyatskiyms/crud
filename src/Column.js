import React from 'react';

class Column extends React.Component {
    handlerChange(event){
        if(!this.props.readOnly){
        let value = event.target.value
        this.props.handlerChange(value, this.props.rowsIndex, this.props.columnKey)
    }}
    render(){
        return (
            <td>
                <input value={this.props.columnDate} onChange={(event)=>this.handlerChange(event)}></input>
            </td>
        )
    }
}
export default Column;
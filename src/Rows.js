import React from 'react';
import Column from './Column';
class Rows extends React.Component {
    constructor(props){
        super(props)
        this.state = {readOnly:true}
        this.handlerChange = this.handlerChange.bind(this);
        this.handlerSave = this.handlerSave.bind(this);
    }
    handlerChange(){
        this.setState({readOnly: !this.state.readOnly})
    }
    handlerSave(){
        this.setState({readOnly: !this.state.readOnly})
        this.props.handlerSave(this.props.rowsDate['_id'], this.props.rowsIndex)
    }
    render(){
        let column = Object.keys(this.props.rowsDate.data).map((key)=>{
            return (<Column 
                columnDate={this.props.rowsDate.data[`${key}`]} 
                rowsIndex = {this.props.rowsIndex}
                columnKey = {key}
                handlerChange = {this.props.handlerChange}
                readOnly={this.state.readOnly}
                key = {key}
                />)
        })
        let buttonChange
        if(this.state.readOnly){
            buttonChange = <button onClick={this.handlerChange}>Редактировать</button>
        }else{
            buttonChange = <button onClick={this.handlerSave}>Сохранить</button>
        }
        return (
        <tr className="rows">
            {column}
            <td>
                <button onClick={() => this.props.handlerRemove(this.props.rowsDate['_id'])}>Удалить</button>
            </td>
            <td>
                {buttonChange}  
            </td>  
        </tr>
        )
    }
}
export default Rows;